export enum Web3ProviderType {
    injected,
    walletConnect
}