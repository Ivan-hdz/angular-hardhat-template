import {BrowserProvider , Eip1193Provider } from 'ethers';

export type InjectedProvider =  BrowserProvider & Eip1193Provider;
