import { Component } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {CdkMenuModule} from '@angular/cdk/menu';
import { Web3Wallet } from '../../services/web3-wallet.service';
import { CommonModule } from '@angular/common';
import { Web3ProviderType } from '../../enums/Web3ProviderType';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    MatButtonModule,
    CommonModule,
    CdkMenuModule
  ],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {
  constructor(public wallet: Web3Wallet) {
  }
  connectInjectedProvider() {
    this.wallet.connect(Web3ProviderType.injected);
  }
}
