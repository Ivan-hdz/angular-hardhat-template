import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Web3ProviderType } from "../enums/Web3ProviderType";
import { BrowserProvider, Signer, ethers } from "ethers";
import { InjectedProvider } from "../types/InjectedProvider";

@Injectable({
    providedIn: 'root'
})
export class Web3Wallet {
    private _injectedProviderSupported = false;
    private _signer = new BehaviorSubject<Signer | undefined>(undefined);
    private _signerAddress = new BehaviorSubject<string | undefined>(undefined);
    private _provider = new BehaviorSubject<InjectedProvider | undefined>(undefined);
    constructor() {
        this._setupInjectedProvider();
        this._setupPropagationOfProviderNext();
    }

    connect(type: Web3ProviderType) {
        if (type === Web3ProviderType.injected && this._injectedProviderSupported) {
            this._connectInjectedProvider();
        } else if (type === Web3ProviderType.walletConnect) {
            this._connectWalletConnectProvider();
        } else {
            this._showNotSupportedError();
        }
    }
    disconnect() {
        this._provider.next(undefined);
    }
    get injectedProviderSupported() {
        return this._injectedProviderSupported
    }
    get signerChange() {
        return this._signer.asObservable();
    }
    get signer() {
        return this._signer.value;
    }
    get signerAddressChange() {
        return this._signerAddress.asObservable();
    }
    get signerAddress() {
        return this._signerAddress.value;
    }
    private async _connectInjectedProvider() {
        const provider = new ethers.BrowserProvider(window.ethereum!);
        this._provider.next(provider as InjectedProvider);
    }
    private _connectWalletConnectProvider() {
        // TODO
    }
    private _showNotSupportedError() {
        // TODO
        console.error('Injected wallet not supported');
    }
    private async _onAccountsChanged(addressSelected: string) {
        this._signer.next(await this._provider.value?.getSigner(addressSelected));
        this._signerAddress.next(await this._signer.value?.getAddress());
        console.log('Account changed to', addressSelected);
    }
    private _setupInjectedProvider() {
        if (window.ethereum !== undefined) {
            this._injectedProviderSupported = true;
            window.ethereum.on('accountsChanged', async (accounts: string[]) => {
                if (accounts.length > 0) {
                    const address = accounts[0];
                    this._onAccountsChanged(address)
                } else {
                    this._provider.next(undefined);
                }
            });
            window.ethereum.request({ method: 'eth_accounts' })
                .then((v: string[]) => {
                    if (v.length > 0) {
                        this._connectInjectedProvider();
                    }
                });
        }
    }
    private _setupPropagationOfProviderNext() {
        this._provider.subscribe(async (provider) => {
            if (provider) {
                this._signer.next(await provider.getSigner());
                this._signerAddress.next(await this._signer.value?.getAddress());
            } else {
                this._signer.next(undefined);
                this._signerAddress.next(undefined);
            }
        });
    }
}