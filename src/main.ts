import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { InjectedProvider } from './app/shared/types/InjectedProvider';

bootstrapApplication(AppComponent, appConfig)
  .catch((err) => console.error(err));

  declare global {
    interface Window {
      ethereum: InjectedProvider | undefined;
    }
  }