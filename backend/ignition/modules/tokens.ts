import { buildModule } from "@nomicfoundation/hardhat-ignition/modules";

export const SimpleDeFiTokenModule = buildModule('SimpleDeFiToken', (m) => {
    const SimpleDeFiToken = m.contract('SimpleDeFiToken');
    return {SimpleDeFiToken};
});