import {expect } from 'chai';
import {ethers} from 'hardhat';
import {toWei} from '../utils/unit-conversion';
import { SimpleDeFiToken } from '../typechain-types';

describe('Simple Defi Token Smart Contract Tests', () => {

    let token: SimpleDeFiToken;
    beforeEach(async () => {
        const factory = await ethers.getContractFactory("SimpleDeFiToken");
        token = await factory.deploy();
    });
    it("Should deploy has correct name, symbol and total supply", async () => {
        expect(await token.name()).to.equal('Simple DeFi Token');
        expect(await token.symbol()).to.equal('SDFT');
        expect(await token.totalSupply()).to.equal(toWei(1000000));
    });
    it('Should transfer 5 tokens from deployer to account 1', async () => {
        const [deployer, address1] = await ethers.getSigners();
        expect(await token.balanceOf(deployer.address)).to.equal(toWei(1000000));
        await token.connect(deployer).transfer(address1.address, toWei(5));
        expect(await token.balanceOf(deployer.address)).to.equal(toWei(1000000 - 5));
        expect( await token.balanceOf(address1.address)).to.equal(toWei(5));
    });
    it('Should not allow to transfer more than balance', async () => {
        const [deployer, address1, address2] = await ethers.getSigners();
        await token.connect(deployer).transfer(address1.address, toWei(5));
        await expect (token.connect(address1).transfer(address2, toWei(10))).to.be.revertedWithCustomError(token, 'ERC20InsufficientBalance');
    })
});