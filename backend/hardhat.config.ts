import dotenv from 'dotenv';
import path from 'path';
import "@nomicfoundation/hardhat-toolbox";

dotenv.config();
const config = {
  solidity: "0.8.24",
  paths: {
    cache: path.join(__dirname, 'cache'),
    artifacts:  path.join(__dirname, 'artifacts'),
    sources:  path.join(__dirname, 'contracts'),
    tests:  path.join(__dirname, 'test'),
    ignition: path.join(__dirname, 'ignition'),
  },
  typechain: {
    outDir: path.join(__dirname, 'typechain-types'),
  },
  networks: {
    sepolia: {
      url: `${process.env['SEPOLIA_API_URL']}`,
      accounts: [
        `${process.env['SEPOLIA_ACCOUNT_PK_1']}`
      ]
    }
  }
};

export default config;
