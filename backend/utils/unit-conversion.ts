export const toWei = (num: number) => {
    return BigInt(num) * BigInt(Math.pow(10, 18));
}
export const fromWei = (num: number) => {
    return BigInt(num) / BigInt(Math.pow(10, 18));
}
