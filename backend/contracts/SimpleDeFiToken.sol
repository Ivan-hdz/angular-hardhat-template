// SPDX-License-Identifier: SEE LICENSE IN LICENSE
pragma solidity 0.8.24;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';

contract SimpleDeFiToken is ERC20 {
    constructor() ERC20('Simple DeFi Token', 'SDFT') {
        _mint(msg.sender, 1e24);
    }
}