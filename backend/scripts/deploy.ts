import { artifacts, ignition } from 'hardhat';
import { SimpleDeFiTokenModule } from '../ignition/modules/tokens';
import fs from 'fs';
import path from 'path';

const CONTRACTS_METADATA_PATH = path.join(__dirname, '..', '..', 'src', 'app', 'shared', 'contracts-metadata');

const saveToFrontend = (name: string, address: string) => {
    if (!fs.existsSync(CONTRACTS_METADATA_PATH)) {
        fs.mkdirSync(CONTRACTS_METADATA_PATH);
    }
    const contractMetadata = artifacts.readArtifactSync(name);
    const contractAddress = { address };
    const addressJsonPath = path.join(CONTRACTS_METADATA_PATH, `${name}-address.json`);
    const metadataJsonPath = path.join(CONTRACTS_METADATA_PATH, `${name}.json`);

    fs.writeFileSync(
        addressJsonPath,
        JSON.stringify(contractAddress, null, 4)
    );
    fs.writeFileSync(
        metadataJsonPath,
        JSON.stringify(contractMetadata, null, 4)
    );
    console.log(
        `${name}-address.json`,
        'created'
    );
    console.log(
        `${name}.json`,
        'created'
    );
}
const deploy = async () => {
    const sdftContract = await ignition.deploy(SimpleDeFiTokenModule);
    const sdftContractAddress = await sdftContract.SimpleDeFiToken.getAddress();
    saveToFrontend(SimpleDeFiTokenModule.id, sdftContractAddress);
    console.log('SimpleDeFiToken deployed at', sdftContractAddress);

}

deploy();